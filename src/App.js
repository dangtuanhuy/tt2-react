import React from 'react';
import logo from './logo.svg';
import './App.css';
import Fruits from './Components/Fruits/Fruits'
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Fruits color='yellow' name='Bananas'></Fruits>
        <Fruits color='Dark blue' name='Watermelons'></Fruits>
        <Fruits color='Orange' name='Oranges'></Fruits>
      </div>
    );
  }
}

export default App;
